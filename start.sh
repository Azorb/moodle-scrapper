#!/bin/bash

dir=$(dirname "$0")
if ! docker ps | grep -q moodle; then
    docker-compose -f $dir/docker-compose.yml up -d > /dev/null 2>&1
    echo "Init..."
    sleep 10
fi
echo "Starting scrapper..."
docker container exec moodle python3 /app/app/cli.py "$@"
