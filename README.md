Léo Dorn, Nicolas Baconnier

# Requirements

- Git
- Docker
- a linux terminal

# Installations

1. Cloner le projet
2. Copier le `.env.example` 

```csharp
cp .env.example .env
```

1. Remplir le  `.env` avec votre `login` et votre `password`
2. Lancer le programme

```csharp
./start.sh
```

## CLI

Options : 

`-nr` : Récupérer seulemeAnt les depots non remis

`-r` : Récupérer seulement les depots remis

Exemple : 

```
./start.sh -nr
```

## Ignore some depots

1. Create a file `ignore.txt` at the project root
2. Add depots ids you want ignore

Exemple : 

```
63108
63225
63114
```

*ignore.txt*