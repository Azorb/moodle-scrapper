import re


class Depot:

    def __init__(self, title: str, status: str, limit_date: str, time_left: str, course_title : str, id : str) -> None:
        self.time_left = None
        self.title = title
        self.set_status(status)
        self.set_limit_date(limit_date)
        self.set_time_left(time_left)
        self.course_title = course_title
        self.id = id

    def __str__(self) -> str:
        return f"Cours : {self.course_title}; Status : {self.status}; Date de dépot : {self.limit_date}; Temps restant : {self.time_left}"

    def set_status(self, status: str) -> None:
        lower_status = status.lower()
        if "remis" in lower_status:
            self.status = "Remis"
        elif "aucune" in lower_status:
            self.status = "Non remis"
        else:
            self.status = status

    def set_time_left(self, time_left: str) -> None:
        splited = time_left.split("de ")
        first_part_lower = splited[0].lower()
        if "retard" in first_part_lower:
            self.time_left = f"Retard : {splited[1]}"
        elif "avance" in first_part_lower:
            self.time_left = f"Avance : {splited[1]}"
        else:
            self.time_left = time_left
        self.time_left = self.time_left.replace("jours","j")
        self.time_left = self.time_left.replace("heures","h")

    def set_limit_date(self,limit_date : str) -> None:
        self.limit_date = limit_date
        self.limit_date = self.limit_date.replace(" janvier","/01")
        self.limit_date = self.limit_date.replace(" février","/02")
        self.limit_date = self.limit_date.replace(" mars","/03")
        self.limit_date = self.limit_date.replace(" avril","/04")
        self.limit_date = self.limit_date.replace(" mai","/05")
        self.limit_date = self.limit_date.replace(" juin","/06")
        self.limit_date = self.limit_date.replace(" juillet","/07")
        self.limit_date = self.limit_date.replace(" aout","/08")
        self.limit_date = self.limit_date.replace(" septembre","/09")
        self.limit_date = self.limit_date.replace(" octobre","/10")
        self.limit_date = self.limit_date.replace(" novembre","/11")
        self.limit_date = self.limit_date.replace(" décembre","/12")
        