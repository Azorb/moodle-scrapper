from decouple import config

from Bot import Bot

if __name__ == '__main__':
    bot = Bot()
    bot.run(config("DISCORD_BOT_TOKEN"))
