from Cookie import Cookie
from Parser import Parser
from Mutex import Mutex
import os
import argparse
import asyncio

cli = argparse.ArgumentParser()
cli.add_argument("-nr", "--non-remis",dest="non_remis", help = "Afficher seulement les devoirs non rendus", action='store_true')
cli.add_argument("-r", "--remis",dest="remis", help = "Afficher seulement les devoirs remis", action='store_true')

args = cli.parse_args()

async def main():
    moodle_session = Cookie.moodle_session()
    depot_list = await Parser.get_depots(moodle_session)
    if args.non_remis:
        depot_list.remove_already_sent()
    if args.remis:
        depot_list.remove_not_sent()
    print(depot_list)

try:
    asyncio.run(main())
finally:
    Mutex.mark_browser_free



# from Parser import Parser;
# from Cookie import Cookie;
# from decouple import config
# import argparse
 
# cli = argparse.ArgumentParser()
# cli.add_argument("-nr", "--non-remis",dest="non_remis", help = "Afficher seulement les devoirs non rendus", action='store_true')
# args = cli.parse_args()

# moodle_session = config("MOODLE_SESSION")
# if not moodle_session:
#     cookie = Cookie()
#     moodle_session = cookie.get_moodle_session()
# print("Moodle session =",moodle_session)
# parser = Parser(moodle_session)
# categories_urls = parser.get_categories_urls()
# courses_urls = parser.get_courses_urls(categories_urls)
# depots_urls = parser.get_depots_urls(courses_urls)
# depot_list = parser.get_all_depots(depots_urls)
# if args.non_remis:
#     depot_list.remove_already_sent()
# depot_list.sort_by_time_left()
# print(depot_list.to_tab())