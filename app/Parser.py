import asyncio
import aiohttp
from typing import *

import requests
from bs4 import BeautifulSoup
import os
from os import path
from Depot import Depot
from DepotList import DepotList


class Parser:
    
    @staticmethod
    async def get_depots(moodle_session) -> DepotList:
        parser = Parser(moodle_session)
        categories_urls = parser.get_categories_urls()
        courses_urls = parser.get_courses_urls(categories_urls)
        cookies = {'MoodleSession': moodle_session}
        async with aiohttp.ClientSession(cookies=cookies) as session:
            depots_urls = await parser.get_depots_urls(courses_urls, session)
            depots = await parser.get_all_depots(depots_urls, session)
            depots.sort_by_time_left()
            return depots
        

    def __init__(self, moodle_session: str) -> None:
        self.moodle_session = moodle_session

    def get_categories_urls(self) -> List[str]:
        result = requests.get("https://moodle.myefrei.fr/course/index.php?categoryid=3163",
                              cookies={'MoodleSession': self.moodle_session})
        soup = BeautifulSoup(result.text, "html.parser")
        elements = soup.find_all("h3", {"class": ["categoryname", "aabtn"]})
        categories_urls = []
        for element in elements:
            for child in element:
                categories_urls.append(child["href"])
        return list(set(categories_urls))

    def get_courses_urls(self, categories_urls: List[str]) -> List[str]:
        courses_urls = []
        for url in categories_urls:
            result = requests.get(url, cookies={'MoodleSession': self.moodle_session})
            soup = BeautifulSoup(result.text, "html.parser")
            elements = soup.find_all("a", {"class": "aalink"})

            for element in elements:
                courses_urls.append(element["href"])
        return list(set(courses_urls))

    async def get_depots_urls(self, courses_urls: List[str], session):
        depots_urls_lists = await asyncio.gather(*[self.get_depot_url(url, session) for url in courses_urls])

        depots_urls = [item for sublist in depots_urls_lists for item in sublist]
        return depots_urls

    async def get_depot_url(self, course_url, session):
        depots_urls = []
        async with session.get(course_url) as result:
            soup = BeautifulSoup(await result.text(), "html.parser")
            title = soup.find("h1",{"id" : "coursetitle"}).getText()
            title = title.split("- ")[1].split(" (")[0]
            elements = soup.find_all("img", {
                "src": "https://moodle.myefrei.fr/theme/image.php/adaptable/assign/1657895457/icon"})
            for element in elements:
                span = element.parent.find("span")
                if element.parent.name == "a" and span and "LSI2" not in span.getText():
                    depot = {
                        "url" : element.parent["href"],
                        "course_title" : title
                    }
                    depots_urls.append(depot)

            return depots_urls

    async def get_all_depots(self, depots_urls, session) -> DepotList:
        depots = []
        ignored_ids = self.get_ignored_ids()
        depots_to_fetch = [depot for depot in depots_urls if depot["url"].split("?id=")[1] not in ignored_ids]
        depots = await asyncio.gather(*[self.get_depot(depot, session) for depot in depots_to_fetch])

        return DepotList(depots)

    async def get_depot(self, depot, session):
        id = depot["url"].split("?id=")[1]
        async with session.get(depot["url"]) as result:
            soup = BeautifulSoup(await result.text(), "html.parser")
            title = soup.find("h2").getText()
            table = soup.find("tbody")
            trs = table.find_all("tr")
            status = trs[0].find("td").getText()
            limit_date = trs[2].find("td").getText()
            time_left = trs[3].find("td").getText()

            return Depot(title, status, limit_date, time_left, depot["course_title"],id)

    def get_ignored_ids(self) -> str:
        directory = os.path.dirname(os.path.realpath(__file__))
        ignore_path = directory + "/../ignore.txt"
        if path.exists(ignore_path):
            file = open(ignore_path)
            return file.read()
        return ""    
